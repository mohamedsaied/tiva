/*
 * File: STM32_PIL_PID.h
 *
 * Code generated for Simulink model 'STM32_PIL_PID'.
 *
 * Model version                  : 1.4
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Mon Apr 22 22:55:59 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_STM32_PIL_PID_h_
#define RTW_HEADER_STM32_PIL_PID_h_
#include <string.h>
#include <stddef.h>
#ifndef STM32_PIL_PID_COMMON_INCLUDES_
# define STM32_PIL_PID_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* STM32_PIL_PID_COMMON_INCLUDES_ */

#include "STM32_PIL_PID_types.h"
#include "MW_target_hardware_resources.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm)  ((rtm)->contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm)         ((rtm)->contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val)    ((rtm)->contStates = (val))
#endif

#ifndef rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag
# define rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm) ((rtm)->CTOutputIncnstWithState)
#endif

#ifndef rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag
# define rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm, val) ((rtm)->CTOutputIncnstWithState = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm)           ((rtm)->intgData)
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val)      ((rtm)->intgData = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm)               ((rtm)->odeF)
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val)          ((rtm)->odeF = (val))
#endif

#ifndef rtmGetOdeY
# define rtmGetOdeY(rtm)               ((rtm)->odeY)
#endif

#ifndef rtmSetOdeY
# define rtmSetOdeY(rtm, val)          ((rtm)->odeY = (val))
#endif

#ifndef rtmGetPeriodicContStateIndices
# define rtmGetPeriodicContStateIndices(rtm) ((rtm)->periodicContStateIndices)
#endif

#ifndef rtmSetPeriodicContStateIndices
# define rtmSetPeriodicContStateIndices(rtm, val) ((rtm)->periodicContStateIndices = (val))
#endif

#ifndef rtmGetPeriodicContStateRanges
# define rtmGetPeriodicContStateRanges(rtm) ((rtm)->periodicContStateRanges)
#endif

#ifndef rtmSetPeriodicContStateRanges
# define rtmSetPeriodicContStateRanges(rtm, val) ((rtm)->periodicContStateRanges = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm)  ((rtm)->zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm)                 ((rtm)->derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val)            ((rtm)->derivs = (val))
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               ((rtm)->Timing.t)
#endif

/* Block signals (default storage) */
typedef struct {
  real_T Sum_p;                        /* '<S86>/Sum' */
} B_STM32_PIL_PID_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T Filter_DSTATE;                /* '<S38>/Filter' */
  real_T Integrator_DSTATE;            /* '<S56>/Integrator' */
} DW_STM32_PIL_PID_T;

/* Continuous states (default storage) */
typedef struct {
  real_T TransferFcn_CSTATE;           /* '<Root>/Transfer Fcn' */
  real_T TransferFcn1_CSTATE;          /* '<Root>/Transfer Fcn1' */
} X_STM32_PIL_PID_T;

/* State derivatives (default storage) */
typedef struct {
  real_T TransferFcn_CSTATE;           /* '<Root>/Transfer Fcn' */
  real_T TransferFcn1_CSTATE;          /* '<Root>/Transfer Fcn1' */
} XDot_STM32_PIL_PID_T;

/* State disabled  */
typedef struct {
  boolean_T TransferFcn_CSTATE;        /* '<Root>/Transfer Fcn' */
  boolean_T TransferFcn1_CSTATE;       /* '<Root>/Transfer Fcn1' */
} XDis_STM32_PIL_PID_T;

#ifndef ODE3_INTG
#define ODE3_INTG

/* ODE3 Integration Data */
typedef struct {
  real_T *y;                           /* output */
  real_T *f[3];                        /* derivatives */
} ODE3_IntgData;

#endif

/* Parameters (default storage) */
struct P_STM32_PIL_PID_T_ {
  real_T DiscretePIDController_D;      /* Mask Parameter: DiscretePIDController_D
                                        * Referenced by: '<S33>/Derivative Gain'
                                        */
  real_T DiscretePIDController_I;      /* Mask Parameter: DiscretePIDController_I
                                        * Referenced by: '<S46>/Integral Gain'
                                        */
  real_T DiscretePIDController_InitialCo;/* Mask Parameter: DiscretePIDController_InitialCo
                                          * Referenced by: '<S38>/Filter'
                                          */
  real_T DiscretePIDController_Initial_p;/* Mask Parameter: DiscretePIDController_Initial_p
                                          * Referenced by: '<S56>/Integrator'
                                          */
  real_T DiscretePIDController_N;      /* Mask Parameter: DiscretePIDController_N
                                        * Referenced by: '<S66>/Filter Coefficient'
                                        */
  real_T DiscretePIDController_P;      /* Mask Parameter: DiscretePIDController_P
                                        * Referenced by: '<S73>/Proportional Gain'
                                        */
  real_T Constant_Value;               /* Expression: 0
                                        * Referenced by: '<Root>/Constant'
                                        */
  real_T TransferFcn_A;                /* Computed Parameter: TransferFcn_A
                                        * Referenced by: '<Root>/Transfer Fcn'
                                        */
  real_T TransferFcn_C;                /* Computed Parameter: TransferFcn_C
                                        * Referenced by: '<Root>/Transfer Fcn'
                                        */
  real_T Step_Time;                    /* Expression: 0
                                        * Referenced by: '<Root>/Step'
                                        */
  real_T Step_Y0;                      /* Expression: 0
                                        * Referenced by: '<Root>/Step'
                                        */
  real_T Step_YFinal;                  /* Expression: 10
                                        * Referenced by: '<Root>/Step'
                                        */
  real_T Filter_gainval;               /* Computed Parameter: Filter_gainval
                                        * Referenced by: '<S38>/Filter'
                                        */
  real_T Integrator_gainval;           /* Computed Parameter: Integrator_gainval
                                        * Referenced by: '<S56>/Integrator'
                                        */
  real_T TransferFcn1_A;               /* Computed Parameter: TransferFcn1_A
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  real_T TransferFcn1_C;               /* Computed Parameter: TransferFcn1_C
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_STM32_PIL_PID_T {
  const char_T *errorStatus;
  RTWSolverInfo solverInfo;
  X_STM32_PIL_PID_T *contStates;
  int_T *periodicContStateIndices;
  real_T *periodicContStateRanges;
  real_T *derivs;
  boolean_T *contStateDisabled;
  boolean_T zCCacheNeedsReset;
  boolean_T derivCacheNeedsReset;
  boolean_T CTOutputIncnstWithState;
  real_T odeY[2];
  real_T odeF[3][2];
  ODE3_IntgData intgData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Block parameters (default storage) */
extern P_STM32_PIL_PID_T STM32_PIL_PID_P;

/* Block signals (default storage) */
extern B_STM32_PIL_PID_T STM32_PIL_PID_B;

/* Continuous states (default storage) */
extern X_STM32_PIL_PID_T STM32_PIL_PID_X;

/* Block states (default storage) */
extern DW_STM32_PIL_PID_T STM32_PIL_PID_DW;

/* Model entry point functions */
extern void STM32_PIL_PID_initialize(void);
extern void STM32_PIL_PID_step(void);
extern void STM32_PIL_PID_terminate(void);

/* Real-time Model object */
extern RT_MODEL_STM32_PIL_PID_T *const STM32_PIL_PID_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Data Type Conversion2' : Unused code path elimination
 * Block '<Root>/Scope' : Unused code path elimination
 * Block '<Root>/Sum1' : Unused code path elimination
 * Block '<Root>/Sum3' : Unused code path elimination
 * Block '<Root>/Data Type Conversion3' : Eliminate redundant data type conversion
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'STM32_PIL_PID'
 * '<S1>'   : 'STM32_PIL_PID/Discrete PID Controller'
 * '<S2>'   : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup'
 * '<S3>'   : 'STM32_PIL_PID/Discrete PID Controller/D Gain'
 * '<S4>'   : 'STM32_PIL_PID/Discrete PID Controller/Filter'
 * '<S5>'   : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs'
 * '<S6>'   : 'STM32_PIL_PID/Discrete PID Controller/I Gain'
 * '<S7>'   : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain'
 * '<S8>'   : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk'
 * '<S9>'   : 'STM32_PIL_PID/Discrete PID Controller/Integrator'
 * '<S10>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator ICs'
 * '<S11>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy'
 * '<S12>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain'
 * '<S13>'  : 'STM32_PIL_PID/Discrete PID Controller/P Copy'
 * '<S14>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain'
 * '<S15>'  : 'STM32_PIL_PID/Discrete PID Controller/Reset Signal'
 * '<S16>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation'
 * '<S17>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation Fdbk'
 * '<S18>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum'
 * '<S19>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum Fdbk'
 * '<S20>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode'
 * '<S21>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode Sum'
 * '<S22>'  : 'STM32_PIL_PID/Discrete PID Controller/postSat Signal'
 * '<S23>'  : 'STM32_PIL_PID/Discrete PID Controller/preSat Signal'
 * '<S24>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Back Calculation'
 * '<S25>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Cont. Clamping Ideal'
 * '<S26>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Cont. Clamping Parallel'
 * '<S27>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Disabled'
 * '<S28>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Disc. Clamping Ideal'
 * '<S29>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Disc. Clamping Parallel'
 * '<S30>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Passthrough'
 * '<S31>'  : 'STM32_PIL_PID/Discrete PID Controller/D Gain/Disabled'
 * '<S32>'  : 'STM32_PIL_PID/Discrete PID Controller/D Gain/External Parameters'
 * '<S33>'  : 'STM32_PIL_PID/Discrete PID Controller/D Gain/Internal Parameters'
 * '<S34>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Cont. Filter'
 * '<S35>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Differentiator'
 * '<S36>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Disabled'
 * '<S37>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Disc. Backward Euler Filter'
 * '<S38>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Disc. Forward Euler Filter'
 * '<S39>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Disc. Trapezoidal Filter'
 * '<S40>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs/Disabled'
 * '<S41>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs/External IC'
 * '<S42>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs/Internal IC - Differentiator'
 * '<S43>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs/Internal IC - Filter'
 * '<S44>'  : 'STM32_PIL_PID/Discrete PID Controller/I Gain/Disabled'
 * '<S45>'  : 'STM32_PIL_PID/Discrete PID Controller/I Gain/External Parameters'
 * '<S46>'  : 'STM32_PIL_PID/Discrete PID Controller/I Gain/Internal Parameters'
 * '<S47>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain/External Parameters'
 * '<S48>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain/Internal Parameters'
 * '<S49>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain/Passthrough'
 * '<S50>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk/Disabled'
 * '<S51>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk/External Parameters'
 * '<S52>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk/Internal Parameters'
 * '<S53>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk/Passthrough'
 * '<S54>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator/Continuous'
 * '<S55>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator/Disabled'
 * '<S56>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator/Discrete'
 * '<S57>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator ICs/Disabled'
 * '<S58>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator ICs/External IC'
 * '<S59>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator ICs/Internal IC'
 * '<S60>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy/Disabled'
 * '<S61>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy/Disabled wSignal Specification'
 * '<S62>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy/External Parameters'
 * '<S63>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy/Internal Parameters'
 * '<S64>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain/Disabled'
 * '<S65>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain/External Parameters'
 * '<S66>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain/Internal Parameters'
 * '<S67>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain/Passthrough'
 * '<S68>'  : 'STM32_PIL_PID/Discrete PID Controller/P Copy/Disabled'
 * '<S69>'  : 'STM32_PIL_PID/Discrete PID Controller/P Copy/External Parameters Ideal'
 * '<S70>'  : 'STM32_PIL_PID/Discrete PID Controller/P Copy/Internal Parameters Ideal'
 * '<S71>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain/Disabled'
 * '<S72>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain/External Parameters'
 * '<S73>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain/Internal Parameters'
 * '<S74>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain/Passthrough'
 * '<S75>'  : 'STM32_PIL_PID/Discrete PID Controller/Reset Signal/Disabled'
 * '<S76>'  : 'STM32_PIL_PID/Discrete PID Controller/Reset Signal/External Reset'
 * '<S77>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation/Enabled'
 * '<S78>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation/Passthrough'
 * '<S79>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation Fdbk/Disabled'
 * '<S80>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation Fdbk/Enabled'
 * '<S81>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation Fdbk/Passthrough'
 * '<S82>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Passthrough_I'
 * '<S83>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Passthrough_P'
 * '<S84>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Sum_PD'
 * '<S85>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Sum_PI'
 * '<S86>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Sum_PID'
 * '<S87>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum Fdbk/Disabled'
 * '<S88>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum Fdbk/Enabled'
 * '<S89>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum Fdbk/Passthrough'
 * '<S90>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode/Disabled'
 * '<S91>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode/Enabled'
 * '<S92>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode Sum/Passthrough'
 * '<S93>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode Sum/Tracking Mode'
 * '<S94>'  : 'STM32_PIL_PID/Discrete PID Controller/postSat Signal/Feedback_Path'
 * '<S95>'  : 'STM32_PIL_PID/Discrete PID Controller/postSat Signal/Forward_Path'
 * '<S96>'  : 'STM32_PIL_PID/Discrete PID Controller/preSat Signal/Feedback_Path'
 * '<S97>'  : 'STM32_PIL_PID/Discrete PID Controller/preSat Signal/Forward_Path'
 */
#endif                                 /* RTW_HEADER_STM32_PIL_PID_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
