/*
 * File: STM32_PIL_PID.c
 *
 * Code generated for Simulink model 'STM32_PIL_PID'.
 *
 * Model version                  : 1.4
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Mon Apr 22 22:55:59 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "STM32_PIL_PID.h"
#include "STM32_PIL_PID_private.h"

/* Block signals (default storage) */
B_STM32_PIL_PID_T STM32_PIL_PID_B;

/* Continuous states */
X_STM32_PIL_PID_T STM32_PIL_PID_X;

/* Block states (default storage) */
DW_STM32_PIL_PID_T STM32_PIL_PID_DW;

/* Real-time model */
RT_MODEL_STM32_PIL_PID_T STM32_PIL_PID_M_;
RT_MODEL_STM32_PIL_PID_T *const STM32_PIL_PID_M = &STM32_PIL_PID_M_;

/*
 * This function updates continuous states using the ODE3 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE3_A[3] = {
    1.0/2.0, 3.0/4.0, 1.0
  };

  static const real_T rt_ODE3_B[3][3] = {
    { 1.0/2.0, 0.0, 0.0 },

    { 0.0, 3.0/4.0, 0.0 },

    { 2.0/9.0, 1.0/3.0, 4.0/9.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE3_IntgData *id = (ODE3_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T hB[3];
  int_T i;
  int_T nXc = 2;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  STM32_PIL_PID_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE3_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[0]);
  rtsiSetdX(si, f1);
  STM32_PIL_PID_step();
  STM32_PIL_PID_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE3_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[1]);
  rtsiSetdX(si, f2);
  STM32_PIL_PID_step();
  STM32_PIL_PID_derivatives();

  /* tnew = t + hA(3);
     ynew = y + f*hB(:,3); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE3_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, tnew);
  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void STM32_PIL_PID_step(void)
{
  /* local block i/o variables */
  real_T rtb_FilterCoefficient;
  real_T rtb_IntegralGain;
  real_T Sum;
  if (rtmIsMajorTimeStep(STM32_PIL_PID_M)) {
    /* set solver stop time */
    rtsiSetSolverStopTime(&STM32_PIL_PID_M->solverInfo,
                          ((STM32_PIL_PID_M->Timing.clockTick0+1)*
      STM32_PIL_PID_M->Timing.stepSize0));
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(STM32_PIL_PID_M)) {
    STM32_PIL_PID_M->Timing.t[0] = rtsiGetT(&STM32_PIL_PID_M->solverInfo);
  }

  /* Step: '<Root>/Step' */
  if (STM32_PIL_PID_M->Timing.t[0] < STM32_PIL_PID_P.Step_Time) {
    Sum = STM32_PIL_PID_P.Step_Y0;
  } else {
    Sum = STM32_PIL_PID_P.Step_YFinal;
  }

  /* End of Step: '<Root>/Step' */

  /* Sum: '<Root>/Sum' incorporates:
   *  Constant: '<Root>/Constant'
   *  Sum: '<Root>/Sum2'
   *  TransferFcn: '<Root>/Transfer Fcn'
   */
  Sum -= STM32_PIL_PID_P.TransferFcn_C * STM32_PIL_PID_X.TransferFcn_CSTATE +
    STM32_PIL_PID_P.Constant_Value;
  if (rtmIsMajorTimeStep(STM32_PIL_PID_M)) {
    /* Gain: '<S66>/Filter Coefficient' incorporates:
     *  DiscreteIntegrator: '<S38>/Filter'
     *  Gain: '<S33>/Derivative Gain'
     *  Sum: '<S38>/SumD'
     */
    rtb_FilterCoefficient = (STM32_PIL_PID_P.DiscretePIDController_D * Sum -
      STM32_PIL_PID_DW.Filter_DSTATE) * STM32_PIL_PID_P.DiscretePIDController_N;

    /* Sum: '<S86>/Sum' incorporates:
     *  DiscreteIntegrator: '<S56>/Integrator'
     *  Gain: '<S73>/Proportional Gain'
     */
    STM32_PIL_PID_B.Sum_p = (STM32_PIL_PID_P.DiscretePIDController_P * Sum +
      STM32_PIL_PID_DW.Integrator_DSTATE) + rtb_FilterCoefficient;

    /* Gain: '<S46>/Integral Gain' */
    rtb_IntegralGain = STM32_PIL_PID_P.DiscretePIDController_I * Sum;
  }

  if (rtmIsMajorTimeStep(STM32_PIL_PID_M)) {
    if (rtmIsMajorTimeStep(STM32_PIL_PID_M)) {
      /* Update for DiscreteIntegrator: '<S38>/Filter' */
      STM32_PIL_PID_DW.Filter_DSTATE += STM32_PIL_PID_P.Filter_gainval *
        rtb_FilterCoefficient;

      /* Update for DiscreteIntegrator: '<S56>/Integrator' */
      STM32_PIL_PID_DW.Integrator_DSTATE += STM32_PIL_PID_P.Integrator_gainval *
        rtb_IntegralGain;
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(STM32_PIL_PID_M)) {
    rt_ertODEUpdateContinuousStates(&STM32_PIL_PID_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     */
    ++STM32_PIL_PID_M->Timing.clockTick0;
    STM32_PIL_PID_M->Timing.t[0] = rtsiGetSolverStopTime
      (&STM32_PIL_PID_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.2s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.2, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       */
      STM32_PIL_PID_M->Timing.clockTick1++;
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void STM32_PIL_PID_derivatives(void)
{
  XDot_STM32_PIL_PID_T *_rtXdot;
  _rtXdot = ((XDot_STM32_PIL_PID_T *) STM32_PIL_PID_M->derivs);

  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn' */
  _rtXdot->TransferFcn_CSTATE = 0.0;
  _rtXdot->TransferFcn_CSTATE += STM32_PIL_PID_P.TransferFcn_A *
    STM32_PIL_PID_X.TransferFcn_CSTATE;
  _rtXdot->TransferFcn_CSTATE += STM32_PIL_PID_B.Sum_p;

  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn1' */
  _rtXdot->TransferFcn1_CSTATE = 0.0;
  _rtXdot->TransferFcn1_CSTATE += STM32_PIL_PID_P.TransferFcn1_A *
    STM32_PIL_PID_X.TransferFcn1_CSTATE;
}

/* Model initialize function */
void STM32_PIL_PID_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)STM32_PIL_PID_M, 0,
                sizeof(RT_MODEL_STM32_PIL_PID_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&STM32_PIL_PID_M->solverInfo,
                          &STM32_PIL_PID_M->Timing.simTimeStep);
    rtsiSetTPtr(&STM32_PIL_PID_M->solverInfo, &rtmGetTPtr(STM32_PIL_PID_M));
    rtsiSetStepSizePtr(&STM32_PIL_PID_M->solverInfo,
                       &STM32_PIL_PID_M->Timing.stepSize0);
    rtsiSetdXPtr(&STM32_PIL_PID_M->solverInfo, &STM32_PIL_PID_M->derivs);
    rtsiSetContStatesPtr(&STM32_PIL_PID_M->solverInfo, (real_T **)
                         &STM32_PIL_PID_M->contStates);
    rtsiSetNumContStatesPtr(&STM32_PIL_PID_M->solverInfo,
      &STM32_PIL_PID_M->Sizes.numContStates);
    rtsiSetNumPeriodicContStatesPtr(&STM32_PIL_PID_M->solverInfo,
      &STM32_PIL_PID_M->Sizes.numPeriodicContStates);
    rtsiSetPeriodicContStateIndicesPtr(&STM32_PIL_PID_M->solverInfo,
      &STM32_PIL_PID_M->periodicContStateIndices);
    rtsiSetPeriodicContStateRangesPtr(&STM32_PIL_PID_M->solverInfo,
      &STM32_PIL_PID_M->periodicContStateRanges);
    rtsiSetErrorStatusPtr(&STM32_PIL_PID_M->solverInfo, (&rtmGetErrorStatus
      (STM32_PIL_PID_M)));
    rtsiSetRTModelPtr(&STM32_PIL_PID_M->solverInfo, STM32_PIL_PID_M);
  }

  rtsiSetSimTimeStep(&STM32_PIL_PID_M->solverInfo, MAJOR_TIME_STEP);
  STM32_PIL_PID_M->intgData.y = STM32_PIL_PID_M->odeY;
  STM32_PIL_PID_M->intgData.f[0] = STM32_PIL_PID_M->odeF[0];
  STM32_PIL_PID_M->intgData.f[1] = STM32_PIL_PID_M->odeF[1];
  STM32_PIL_PID_M->intgData.f[2] = STM32_PIL_PID_M->odeF[2];
  STM32_PIL_PID_M->contStates = ((X_STM32_PIL_PID_T *) &STM32_PIL_PID_X);
  rtsiSetSolverData(&STM32_PIL_PID_M->solverInfo, (void *)
                    &STM32_PIL_PID_M->intgData);
  rtsiSetSolverName(&STM32_PIL_PID_M->solverInfo,"ode3");
  rtmSetTPtr(STM32_PIL_PID_M, &STM32_PIL_PID_M->Timing.tArray[0]);
  STM32_PIL_PID_M->Timing.stepSize0 = 0.2;

  /* block I/O */
  (void) memset(((void *) &STM32_PIL_PID_B), 0,
                sizeof(B_STM32_PIL_PID_T));

  /* states (continuous) */
  {
    (void) memset((void *)&STM32_PIL_PID_X, 0,
                  sizeof(X_STM32_PIL_PID_T));
  }

  /* states (dwork) */
  (void) memset((void *)&STM32_PIL_PID_DW, 0,
                sizeof(DW_STM32_PIL_PID_T));

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn' */
  STM32_PIL_PID_X.TransferFcn_CSTATE = 0.0;

  /* InitializeConditions for DiscreteIntegrator: '<S38>/Filter' */
  STM32_PIL_PID_DW.Filter_DSTATE =
    STM32_PIL_PID_P.DiscretePIDController_InitialCo;

  /* InitializeConditions for DiscreteIntegrator: '<S56>/Integrator' */
  STM32_PIL_PID_DW.Integrator_DSTATE =
    STM32_PIL_PID_P.DiscretePIDController_Initial_p;

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn1' */
  STM32_PIL_PID_X.TransferFcn1_CSTATE = 0.0;
}

/* Model terminate function */
void STM32_PIL_PID_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
