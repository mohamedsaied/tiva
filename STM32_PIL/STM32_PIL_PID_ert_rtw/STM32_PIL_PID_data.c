/*
 * File: STM32_PIL_PID_data.c
 *
 * Code generated for Simulink model 'STM32_PIL_PID'.
 *
 * Model version                  : 1.4
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Mon Apr 22 22:55:59 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "STM32_PIL_PID.h"
#include "STM32_PIL_PID_private.h"

/* Block parameters (default storage) */
P_STM32_PIL_PID_T STM32_PIL_PID_P = {
  /* Mask Parameter: DiscretePIDController_D
   * Referenced by: '<S33>/Derivative Gain'
   */
  -0.117318190380174,

  /* Mask Parameter: DiscretePIDController_I
   * Referenced by: '<S46>/Integral Gain'
   */
  0.946669571336822,

  /* Mask Parameter: DiscretePIDController_InitialCo
   * Referenced by: '<S38>/Filter'
   */
  0.0,

  /* Mask Parameter: DiscretePIDController_Initial_p
   * Referenced by: '<S56>/Integrator'
   */
  0.0,

  /* Mask Parameter: DiscretePIDController_N
   * Referenced by: '<S66>/Filter Coefficient'
   */
  1.35004736212021,

  /* Mask Parameter: DiscretePIDController_P
   * Referenced by: '<S73>/Proportional Gain'
   */
  0.884036721518841,

  /* Expression: 0
   * Referenced by: '<Root>/Constant'
   */
  0.0,

  /* Computed Parameter: TransferFcn_A
   * Referenced by: '<Root>/Transfer Fcn'
   */
  -1.0,

  /* Computed Parameter: TransferFcn_C
   * Referenced by: '<Root>/Transfer Fcn'
   */
  1.0,

  /* Expression: 0
   * Referenced by: '<Root>/Step'
   */
  0.0,

  /* Expression: 0
   * Referenced by: '<Root>/Step'
   */
  0.0,

  /* Expression: 10
   * Referenced by: '<Root>/Step'
   */
  10.0,

  /* Computed Parameter: Filter_gainval
   * Referenced by: '<S38>/Filter'
   */
  0.2,

  /* Computed Parameter: Integrator_gainval
   * Referenced by: '<S56>/Integrator'
   */
  0.2,

  /* Computed Parameter: TransferFcn1_A
   * Referenced by: '<Root>/Transfer Fcn1'
   */
  -1.0,

  /* Computed Parameter: TransferFcn1_C
   * Referenced by: '<Root>/Transfer Fcn1'
   */
  1.0
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
