/*
 * File: STM32_PIL_PID_types.h
 *
 * Code generated for Simulink model 'STM32_PIL_PID'.
 *
 * Model version                  : 1.4
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Mon Apr 22 22:55:59 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_STM32_PIL_PID_types_h_
#define RTW_HEADER_STM32_PIL_PID_types_h_
#include "rtwtypes.h"

/* Parameters (default storage) */
typedef struct P_STM32_PIL_PID_T_ P_STM32_PIL_PID_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_STM32_PIL_PID_T RT_MODEL_STM32_PIL_PID_T;

#endif                                 /* RTW_HEADER_STM32_PIL_PID_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
