/*
 * File: Discrete_types.h
 *
 * Code generated for Simulink model 'Discrete'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Mon Apr 22 23:03:29 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Discrete_types_h_
#define RTW_HEADER_Discrete_types_h_
#include "rtwtypes.h"

/* Parameters (default storage) */
typedef struct P_Discrete_T_ P_Discrete_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_Discrete_T RT_MODEL_Discrete_T;

#endif                                 /* RTW_HEADER_Discrete_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
