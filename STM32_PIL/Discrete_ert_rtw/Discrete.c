/*
 * File: Discrete.c
 *
 * Code generated for Simulink model 'Discrete'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Mon Apr 22 23:03:29 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Discrete.h"
#include "Discrete_private.h"

/* Block states (default storage) */
DW_Discrete_T Discrete_DW;

/* External inputs (root inport signals with default storage) */
ExtU_Discrete_T Discrete_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_Discrete_T Discrete_Y;

/* Real-time model */
RT_MODEL_Discrete_T Discrete_M_;
RT_MODEL_Discrete_T *const Discrete_M = &Discrete_M_;

/* Model step function */
void Discrete_step(void)
{
  real_T rtb_FilterCoefficient;

  /* Gain: '<S66>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S38>/Filter'
   *  Gain: '<S33>/Derivative Gain'
   *  Inport: '<Root>/u'
   *  Sum: '<S38>/SumD'
   */
  rtb_FilterCoefficient = (Discrete_P.DiscretePIDController_D * Discrete_U.u -
    Discrete_DW.Filter_DSTATE) * Discrete_P.DiscretePIDController_N;

  /* Outport: '<Root>/y' incorporates:
   *  DiscreteIntegrator: '<S56>/Integrator'
   *  Gain: '<S73>/Proportional Gain'
   *  Inport: '<Root>/u'
   *  Sum: '<S86>/Sum'
   */
  Discrete_Y.y = (Discrete_P.DiscretePIDController_P * Discrete_U.u +
                  Discrete_DW.Integrator_DSTATE) + rtb_FilterCoefficient;

  /* Update for DiscreteIntegrator: '<S56>/Integrator' incorporates:
   *  Gain: '<S46>/Integral Gain'
   *  Inport: '<Root>/u'
   */
  Discrete_DW.Integrator_DSTATE += Discrete_P.DiscretePIDController_I *
    Discrete_U.u * Discrete_P.Integrator_gainval;

  /* Update for DiscreteIntegrator: '<S38>/Filter' */
  Discrete_DW.Filter_DSTATE += Discrete_P.Filter_gainval * rtb_FilterCoefficient;
}

/* Model initialize function */
void Discrete_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(Discrete_M, (NULL));

  /* states (dwork) */
  (void) memset((void *)&Discrete_DW, 0,
                sizeof(DW_Discrete_T));

  /* external inputs */
  Discrete_U.u = 0.0;

  /* external outputs */
  Discrete_Y.y = 0.0;

  /* InitializeConditions for DiscreteIntegrator: '<S56>/Integrator' */
  Discrete_DW.Integrator_DSTATE = Discrete_P.DiscretePIDController_Initial_p;

  /* InitializeConditions for DiscreteIntegrator: '<S38>/Filter' */
  Discrete_DW.Filter_DSTATE = Discrete_P.DiscretePIDController_InitialCo;
}

/* Model terminate function */
void Discrete_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
