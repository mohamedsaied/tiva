/*
 * File: Discrete_data.c
 *
 * Code generated for Simulink model 'Discrete'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Mon Apr 22 23:03:29 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Discrete.h"
#include "Discrete_private.h"

/* Block parameters (default storage) */
P_Discrete_T Discrete_P = {
  /* Mask Parameter: DiscretePIDController_D
   * Referenced by: '<S33>/Derivative Gain'
   */
  -0.117318190380174,

  /* Mask Parameter: DiscretePIDController_I
   * Referenced by: '<S46>/Integral Gain'
   */
  0.946669571336822,

  /* Mask Parameter: DiscretePIDController_InitialCo
   * Referenced by: '<S38>/Filter'
   */
  0.0,

  /* Mask Parameter: DiscretePIDController_Initial_p
   * Referenced by: '<S56>/Integrator'
   */
  0.0,

  /* Mask Parameter: DiscretePIDController_N
   * Referenced by: '<S66>/Filter Coefficient'
   */
  1.35004736212021,

  /* Mask Parameter: DiscretePIDController_P
   * Referenced by: '<S73>/Proportional Gain'
   */
  0.884036721518841,

  /* Computed Parameter: Integrator_gainval
   * Referenced by: '<S56>/Integrator'
   */
  0.2,

  /* Computed Parameter: Filter_gainval
   * Referenced by: '<S38>/Filter'
   */
  0.2
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
