/*
 * File: Discrete_ca.h
 *
 * Abstract: Tests assumptions in the generated code.
 */

#ifndef DISCRETE_CA_H
#define DISCRETE_CA_H

/* preprocessor validation checks */
#include "Discrete_ca_preproc.h"
#include "coder_assumptions_hwimpl.h"

/* variables holding test results */
extern CA_HWImpl_TestResults CA_Discrete_HWRes;
extern CA_PWS_TestResults CA_Discrete_PWSRes;

/* variables holding "expected" and "actual" hardware implementation */
extern const CA_HWImpl CA_Discrete_ExpHW;
extern CA_HWImpl CA_Discrete_ActHW;

/* entry point function to run tests */
void Discrete_caRunTests(void);

#endif                                 /* DISCRETE_CA_H */
