/*
 * File: Discrete.h
 *
 * Code generated for Simulink model 'Discrete'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Mon Apr 22 23:03:29 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Discrete_h_
#define RTW_HEADER_Discrete_h_
#include <stddef.h>
#include <string.h>
#ifndef Discrete_COMMON_INCLUDES_
# define Discrete_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* Discrete_COMMON_INCLUDES_ */

#include "Discrete_types.h"
#include "MW_target_hardware_resources.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T Integrator_DSTATE;            /* '<S56>/Integrator' */
  real_T Filter_DSTATE;                /* '<S38>/Filter' */
} DW_Discrete_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T u;                            /* '<Root>/u' */
} ExtU_Discrete_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T y;                            /* '<Root>/y' */
} ExtY_Discrete_T;

/* Parameters (default storage) */
struct P_Discrete_T_ {
  real_T DiscretePIDController_D;      /* Mask Parameter: DiscretePIDController_D
                                        * Referenced by: '<S33>/Derivative Gain'
                                        */
  real_T DiscretePIDController_I;      /* Mask Parameter: DiscretePIDController_I
                                        * Referenced by: '<S46>/Integral Gain'
                                        */
  real_T DiscretePIDController_InitialCo;/* Mask Parameter: DiscretePIDController_InitialCo
                                          * Referenced by: '<S38>/Filter'
                                          */
  real_T DiscretePIDController_Initial_p;/* Mask Parameter: DiscretePIDController_Initial_p
                                          * Referenced by: '<S56>/Integrator'
                                          */
  real_T DiscretePIDController_N;      /* Mask Parameter: DiscretePIDController_N
                                        * Referenced by: '<S66>/Filter Coefficient'
                                        */
  real_T DiscretePIDController_P;      /* Mask Parameter: DiscretePIDController_P
                                        * Referenced by: '<S73>/Proportional Gain'
                                        */
  real_T Integrator_gainval;           /* Computed Parameter: Integrator_gainval
                                        * Referenced by: '<S56>/Integrator'
                                        */
  real_T Filter_gainval;               /* Computed Parameter: Filter_gainval
                                        * Referenced by: '<S38>/Filter'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_Discrete_T {
  const char_T *errorStatus;
};

/* Block parameters (default storage) */
extern P_Discrete_T Discrete_P;

/* Block states (default storage) */
extern DW_Discrete_T Discrete_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_Discrete_T Discrete_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_Discrete_T Discrete_Y;

/* Model entry point functions */
extern void Discrete_initialize(void);
extern void Discrete_step(void);
extern void Discrete_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Discrete_T *const Discrete_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Note that this particular code originates from a subsystem build,
 * and has its own system numbers different from the parent model.
 * Refer to the system hierarchy for this subsystem below, and use the
 * MATLAB hilite_system command to trace the generated code back
 * to the parent model.  For example,
 *
 * hilite_system('STM32_PIL_PID/Discrete PID Controller')    - opens subsystem STM32_PIL_PID/Discrete PID Controller
 * hilite_system('STM32_PIL_PID/Discrete PID Controller/Kp') - opens and selects block Kp
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'STM32_PIL_PID'
 * '<S1>'   : 'STM32_PIL_PID/Discrete PID Controller'
 * '<S2>'   : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup'
 * '<S3>'   : 'STM32_PIL_PID/Discrete PID Controller/D Gain'
 * '<S4>'   : 'STM32_PIL_PID/Discrete PID Controller/Filter'
 * '<S5>'   : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs'
 * '<S6>'   : 'STM32_PIL_PID/Discrete PID Controller/I Gain'
 * '<S7>'   : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain'
 * '<S8>'   : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk'
 * '<S9>'   : 'STM32_PIL_PID/Discrete PID Controller/Integrator'
 * '<S10>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator ICs'
 * '<S11>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy'
 * '<S12>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain'
 * '<S13>'  : 'STM32_PIL_PID/Discrete PID Controller/P Copy'
 * '<S14>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain'
 * '<S15>'  : 'STM32_PIL_PID/Discrete PID Controller/Reset Signal'
 * '<S16>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation'
 * '<S17>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation Fdbk'
 * '<S18>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum'
 * '<S19>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum Fdbk'
 * '<S20>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode'
 * '<S21>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode Sum'
 * '<S22>'  : 'STM32_PIL_PID/Discrete PID Controller/postSat Signal'
 * '<S23>'  : 'STM32_PIL_PID/Discrete PID Controller/preSat Signal'
 * '<S24>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Back Calculation'
 * '<S25>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Cont. Clamping Ideal'
 * '<S26>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Cont. Clamping Parallel'
 * '<S27>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Disabled'
 * '<S28>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Disc. Clamping Ideal'
 * '<S29>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Disc. Clamping Parallel'
 * '<S30>'  : 'STM32_PIL_PID/Discrete PID Controller/Anti-windup/Passthrough'
 * '<S31>'  : 'STM32_PIL_PID/Discrete PID Controller/D Gain/Disabled'
 * '<S32>'  : 'STM32_PIL_PID/Discrete PID Controller/D Gain/External Parameters'
 * '<S33>'  : 'STM32_PIL_PID/Discrete PID Controller/D Gain/Internal Parameters'
 * '<S34>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Cont. Filter'
 * '<S35>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Differentiator'
 * '<S36>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Disabled'
 * '<S37>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Disc. Backward Euler Filter'
 * '<S38>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Disc. Forward Euler Filter'
 * '<S39>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter/Disc. Trapezoidal Filter'
 * '<S40>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs/Disabled'
 * '<S41>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs/External IC'
 * '<S42>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs/Internal IC - Differentiator'
 * '<S43>'  : 'STM32_PIL_PID/Discrete PID Controller/Filter ICs/Internal IC - Filter'
 * '<S44>'  : 'STM32_PIL_PID/Discrete PID Controller/I Gain/Disabled'
 * '<S45>'  : 'STM32_PIL_PID/Discrete PID Controller/I Gain/External Parameters'
 * '<S46>'  : 'STM32_PIL_PID/Discrete PID Controller/I Gain/Internal Parameters'
 * '<S47>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain/External Parameters'
 * '<S48>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain/Internal Parameters'
 * '<S49>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain/Passthrough'
 * '<S50>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk/Disabled'
 * '<S51>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk/External Parameters'
 * '<S52>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk/Internal Parameters'
 * '<S53>'  : 'STM32_PIL_PID/Discrete PID Controller/Ideal P Gain Fdbk/Passthrough'
 * '<S54>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator/Continuous'
 * '<S55>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator/Disabled'
 * '<S56>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator/Discrete'
 * '<S57>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator ICs/Disabled'
 * '<S58>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator ICs/External IC'
 * '<S59>'  : 'STM32_PIL_PID/Discrete PID Controller/Integrator ICs/Internal IC'
 * '<S60>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy/Disabled'
 * '<S61>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy/Disabled wSignal Specification'
 * '<S62>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy/External Parameters'
 * '<S63>'  : 'STM32_PIL_PID/Discrete PID Controller/N Copy/Internal Parameters'
 * '<S64>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain/Disabled'
 * '<S65>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain/External Parameters'
 * '<S66>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain/Internal Parameters'
 * '<S67>'  : 'STM32_PIL_PID/Discrete PID Controller/N Gain/Passthrough'
 * '<S68>'  : 'STM32_PIL_PID/Discrete PID Controller/P Copy/Disabled'
 * '<S69>'  : 'STM32_PIL_PID/Discrete PID Controller/P Copy/External Parameters Ideal'
 * '<S70>'  : 'STM32_PIL_PID/Discrete PID Controller/P Copy/Internal Parameters Ideal'
 * '<S71>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain/Disabled'
 * '<S72>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain/External Parameters'
 * '<S73>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain/Internal Parameters'
 * '<S74>'  : 'STM32_PIL_PID/Discrete PID Controller/Parallel P Gain/Passthrough'
 * '<S75>'  : 'STM32_PIL_PID/Discrete PID Controller/Reset Signal/Disabled'
 * '<S76>'  : 'STM32_PIL_PID/Discrete PID Controller/Reset Signal/External Reset'
 * '<S77>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation/Enabled'
 * '<S78>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation/Passthrough'
 * '<S79>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation Fdbk/Disabled'
 * '<S80>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation Fdbk/Enabled'
 * '<S81>'  : 'STM32_PIL_PID/Discrete PID Controller/Saturation Fdbk/Passthrough'
 * '<S82>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Passthrough_I'
 * '<S83>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Passthrough_P'
 * '<S84>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Sum_PD'
 * '<S85>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Sum_PI'
 * '<S86>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum/Sum_PID'
 * '<S87>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum Fdbk/Disabled'
 * '<S88>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum Fdbk/Enabled'
 * '<S89>'  : 'STM32_PIL_PID/Discrete PID Controller/Sum Fdbk/Passthrough'
 * '<S90>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode/Disabled'
 * '<S91>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode/Enabled'
 * '<S92>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode Sum/Passthrough'
 * '<S93>'  : 'STM32_PIL_PID/Discrete PID Controller/Tracking Mode Sum/Tracking Mode'
 * '<S94>'  : 'STM32_PIL_PID/Discrete PID Controller/postSat Signal/Feedback_Path'
 * '<S95>'  : 'STM32_PIL_PID/Discrete PID Controller/postSat Signal/Forward_Path'
 * '<S96>'  : 'STM32_PIL_PID/Discrete PID Controller/preSat Signal/Feedback_Path'
 * '<S97>'  : 'STM32_PIL_PID/Discrete PID Controller/preSat Signal/Forward_Path'
 */
#endif                                 /* RTW_HEADER_Discrete_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
